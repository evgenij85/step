<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fonts/font-awesome-4.7.0/css/font-awesome.min.css',
        //'css/animate/animate.css',
        //'css/css-hamburgers/hamburgers.min.css',
        //'css/animsition/css/animsition.min.css',
        //'css/select2/select2.min.css',
        //'css/slick/slick.css',
        //'css/MagnificPopup/magnific-popup.css',
        'css/util.min.css',
        'css/main.css',

    ];
    public $js = [
        //'js/animsition/js/animsition.min.js',
        //'js/bootstrap/js/popper.js',
        //'js/select2/select2.min.js',
        //'js/parallax100/parallax100.js',
        // 'js/waypoint/jquery.waypoints.min.js',
        // 'js/countterup/jquery.counterup.min.js',
        // 'js/slick/slick.min.js',
        'js/slick-custom.js',
        //  'js/MagnificPopup/jquery.magnific-popup.min.js',
        // 'js/isotope/isotope.pkgd.min.js',
        'js/main.js'

    ];


    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
