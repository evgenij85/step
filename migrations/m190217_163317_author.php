<?php

use yii\db\Migration;

/**
 * Class m190217_163317_author
 */
class m190217_163317_author extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE {{%author}} (
                `author_id` INT(11) NOT NULL AUTO_INCREMENT,
                `author_name` VARCHAR(255) NULL DEFAULT NULL,
                PRIMARY KEY (`author_id`)
            )
            COMMENT='авторы книги'
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            ;
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190217_163317_author cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_163317_author cannot be reverted.\n";

        return false;
    }
    */
}
