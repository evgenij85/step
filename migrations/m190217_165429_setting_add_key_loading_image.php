<?php

use yii\db\Migration;

/**
 * Class m190217_165429_setting_add_key_loading_image
 */
class m190217_165429_setting_add_key_loading_image extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
           INSERT INTO {{%settings}} (`settings_key`, `settings_value`, `settings_description`) VALUES
           ('loading_image', '1', 'куда загружаем картинки. 1-локально, 2 -удаленно');
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190217_165429_setting_add_key_loading_image cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_165429_setting_add_key_loading_image cannot be reverted.\n";

        return false;
    }
    */
}
