<?php

use yii\db\Migration;

/**
 * Class m190217_163444_settings
 */
class m190217_163444_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
           CREATE TABLE {{%settings}} (
                `settings_id` INT(11) NOT NULL AUTO_INCREMENT,
                `settings_key` VARCHAR(50) NOT NULL,
                `settings_value` VARCHAR(50) NOT NULL,
                `settings_description` VARCHAR(50) NULL DEFAULT NULL,
                PRIMARY KEY (`settings_id`)
            )
            COMMENT='настройки '
            COLLATE='utf8_general_ci'
            ENGINE=MyISAM
            ;

        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190217_163444_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_163444_settings cannot be reverted.\n";

        return false;
    }
    */
}
