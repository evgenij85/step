<?php

use yii\db\Migration;

/**
 * Class m190217_163430_rubrics
 */
class m190217_163430_rubrics extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
           CREATE TABLE {{%rubrics}} (
                `rubrics_id` INT(11) NOT NULL AUTO_INCREMENT,
                `rubrics_parent_id` INT(11) NULL DEFAULT '0' COMMENT 'id родителя рубрики',
                `rubrics_name` VARCHAR(255) NOT NULL COMMENT 'название рубрики',
                PRIMARY KEY (`rubrics_id`),
                INDEX `rubrics_parent` (`rubrics_parent_id`)
            )
            COMMENT='Рубрики книг'
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            ;
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190217_163430_rubrics cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_163430_rubrics cannot be reverted.\n";

        return false;
    }
    */
}
