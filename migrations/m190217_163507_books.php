<?php

use yii\db\Migration;

/**
 * Class m190217_163507_books
 */
class m190217_163507_books extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
           CREATE TABLE {{%books}} (
                `books_id` INT(11) NOT NULL AUTO_INCREMENT,
                `books_rubrics_id` INT(11) NOT NULL COMMENT 'id рубрики',
                `books_author_id` INT(11) NOT NULL COMMENT 'id автора книги',
                `books_publishing_id` INT(11) NOT NULL COMMENT 'id издательства',
                `books_name` VARCHAR(255) NOT NULL COMMENT 'название книги',
                `books_date_publishing` DATE NULL DEFAULT NULL COMMENT 'дата издательства',
                PRIMARY KEY (`books_id`),
                INDEX `FK_RUBRICK` (`books_rubrics_id`),
                INDEX `FK_AUTHOR` (`books_author_id`),
                INDEX `FK_PUBLISHING` (`books_publishing_id`),
                CONSTRAINT `FK_AUTHOR` FOREIGN KEY (`books_author_id`) REFERENCES {{%author}} (`author_id`),
                CONSTRAINT `FK_PUBLISHING` FOREIGN KEY (`books_publishing_id`) REFERENCES {{%publishing}} (`publishing_id`),
                CONSTRAINT `FK_RUBRICK` FOREIGN KEY (`books_rubrics_id`) REFERENCES {{%rubrics}} (`rubrics_id`)
            )
            COMMENT='Каталог книг'
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            ;
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190217_163507_books cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_163507_books cannot be reverted.\n";

        return false;
    }
    */
}
