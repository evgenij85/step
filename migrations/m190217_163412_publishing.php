<?php

use yii\db\Migration;

/**
 * Class m190217_163412_publishing
 */
class m190217_163412_publishing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
           CREATE TABLE {{%publishing}} (
                `publishing_id` INT(11) NOT NULL AUTO_INCREMENT,
                `publishing_name` VARCHAR(255) NULL DEFAULT NULL COMMENT 'названия издательства',
                `publishing_address` VARCHAR(255) NULL DEFAULT NULL COMMENT 'адрес издательства',
                `address_phone` VARCHAR(255) NULL DEFAULT NULL COMMENT 'телефон издательства',
                PRIMARY KEY (`publishing_id`)
            )
            COMMENT='Издательство '
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB          
            ;
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190217_163412_publishing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_163412_publishing cannot be reverted.\n";

        return false;
    }
    */
}
