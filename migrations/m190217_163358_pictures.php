<?php

use yii\db\Migration;

/**
 * Class m190217_163358_pictures
 */
class m190217_163358_pictures extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
           CREATE TABLE {{%pictures}} (
                `pictures_id` INT(11) NOT NULL AUTO_INCREMENT,
                `pictures_books_id` INT(11) NOT NULL COMMENT 'id книги ',
                `pictures_name` VARCHAR(255) NOT NULL COMMENT 'имя картинки',
                `pictures_server` TINYINT(4) NOT NULL COMMENT 'Размещение картинки, 1 локально 2 - удаленный сервер',
                PRIMARY KEY (`pictures_id`),
                INDEX `pictures_books_id` (`pictures_books_id`)
            )
            COMMENT='Картинки книг'
            COLLATE='utf8_general_ci'
            ENGINE=MyISAM
            ;
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190217_163358_pictures cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_163358_pictures cannot be reverted.\n";

        return false;
    }
    */
}
