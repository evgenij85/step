<?php

namespace app\modules\admin\controllers;

use app\models\Settings;
use Yii;
use app\models\Author;
use app\models\search\AuthorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuthorController implements the CRUD actions for Author model.
 */
class SettingsController extends Controller
{

    /**
     * Lists all Author models.
     * @return mixed
     */
    public function actionIndex()
    {
        if($post = Yii::$app->request->post()){
            if(Settings::updateValues('loading_image', $post['loading_image'])){
                Yii::$app->session->setFlash('success', 'Изменения сохранены');
            }else{
                Yii::$app->session->setFlash('error','Ошибка сохранения');
            }
        }

        return $this->render('index');
    }


}
