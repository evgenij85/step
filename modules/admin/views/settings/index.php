<?php
use yii\helpers\Html;
use \app\models\Settings;

?>
<h3>Куда загружаем картинки ? </h3>
<?= Html::beginForm() ?>
<?= Html::radio('loading_image', Settings::activityValues('loading_image','1'), ['value'=>1,'label' => 'Локально']) ?><br>
<?= Html::radio('loading_image', Settings::activityValues('loading_image','2'), ['value'=>2,'label' => 'Удаленный сервер']) ?>
<br><br>
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
<?=Html::endForm()?>

