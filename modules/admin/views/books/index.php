<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Books', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'books_id',
            [
                'attribute'=>'books_rubrics_id',
                'value'=>function($data){
                    return $data->booksRubrics->rubrics_name;
                },
            ],
            [
                'attribute'=>'books_author_id',
                'value'=>function($data){
                    return $data->booksAuthor->author_name;
                },
            ],
            [
                'attribute'=>'books_publishing_id',
                'value'=>function($data){
                    return $data->booksPublishing->publishing_name;
                },
            ],
            'books_name',
            'books_date_publishing',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
