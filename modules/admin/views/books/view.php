<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = $model->books_id;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="books-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->books_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->books_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'books_id',
            [
                'attribute'=>'books_rubrics_id',
                'value'=>function($data){
                    return $data->booksRubrics->rubrics_name;
                },
            ],
            [
                'attribute'=>'books_author_id',
                'value'=>function($data){
                    return $data->booksAuthor->author_name;
                },
            ],
            [
                'attribute'=>'books_publishing_id',
                'value'=>function($data){
                    return $data->booksPublishing->publishing_name;
                },
            ],
            'books_name',
            'books_date_publishing',
            [
                'label'=>'Картинки',
                'format'=>'raw', // Возможные варианты: raw, html
                'value'=>function($data){
                    return \app\models\Pictures::getListViewAdmin($data->books_id);
                }
            ],
        ],
    ]) ?>


</div>



<?php
include_once 'js_included.php';
?>