<?php
$js = <<<JS
    $('.action-delete').click(function() {
        var id =  $(this).attr('data-id');
        $.ajax({
            url: 'delete-image',
            data: "id="+$(this).attr('data-id'),
            type: "POST",
            dataType: "json",
            success: function(msg){
                alert('Успешно удален');
                $('#pic_id_'+id).css('display','none');
            }

        });
    });

JS;
$this->registerJs($js, \yii\web\View::POS_END);
?>
