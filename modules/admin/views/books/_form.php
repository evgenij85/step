<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="books-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'books_name')->textInput(['maxlength' => true])->label('Название книги') ?>

    <?= $form->field($model, 'books_rubrics_id')->dropDownList(\app\models\Rubrics::getList(),
        [
            'prompt' => 'Выберите рубрику'
        ]
    )->label('Рубрика')?>

    <?= $form->field($model, 'books_author_id')->dropDownList(\app\models\Author::getList(),
        [
            'prompt' => 'Выберите автора'
        ]
    )->label('Автор')?>

    <?= $form->field($model, 'books_publishing_id')->dropDownList(\app\models\Publishing::getList(),
        [
            'prompt' =>  'Выберите издательство',
        ]
    )->label('Издательство')?>

    <label>Дата публикации</label>
    <?= $form->field($model, 'books_date_publishing')->widget(\yii\jui\DatePicker::class, [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])->label('') ?>

    <?= $form->field($model, 'image[]')->fileInput(['multiple' => true])->label('Картинки') ?>

    <?php
      if(!$model->isNewRecord){
         echo \app\models\Pictures::getListViewAdmin(Yii::$app->request->get('id'));
      }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
include_once 'js_included.php';
?>