<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rubrics */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="rubrics-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php // $form->field($model, 'rubrics_parent_id')->textInput() ?>

    <?= $form->field($model, 'rubrics_parent_id')->dropDownList(\app\models\Rubrics::getList(), /* доступные к просмотру страны, в модели стран.*/
        [
                'prompt' => [
                    'text' => 'Без рубрики (главная)',
                    'options' => [
                        'value' => '0',
                    ]
                ]

        ]
    )?>

    <?= $form->field($model, 'rubrics_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
