<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rubrics */

$this->title = 'Update Rubrics: ' . $model->rubrics_id;
$this->params['breadcrumbs'][] = ['label' => 'Rubrics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rubrics_id, 'url' => ['view', 'id' => $model->rubrics_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rubrics-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
