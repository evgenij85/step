<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Publishing */

$this->title = 'Update Publishing: ' . $model->publishing_id;
$this->params['breadcrumbs'][] = ['label' => 'Publishings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->publishing_id, 'url' => ['view', 'id' => $model->publishing_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="publishing-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
