<?php

namespace app\models\form;

use app\models\Books;
use app\models\Pictures;
use app\models\Settings;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFiles;
    public $booksId;



    public function rules()
    {
        return [
            [['imageFiles'], 'file',
                'skipOnEmpty' => false,
                'extensions' => 'png, jpg',
                'maxSize' => 1024 * 1024 * 1024,
                'maxFiles' => 10
            ],
        ];
    }

    public static function download(Books $models)
    {
        $model = new self();
        $model->booksId = $models->books_id;
        $model->imageFiles = UploadedFile::getInstances($models, 'image');
        if ($model->upload()) {
            return true;
        }

        return false;
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                if($this->exportFile($file)){
                    Pictures::addNew($this->booksId, $this->fileName($file));
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /*
     * Путь куда сохраняем картинки
     */
    public function exportFile($file)
    {
        if(Settings::getValue('loading_image') == '1'){
            return $file->saveAs(\Yii::getAlias('@webUploadsSite').DIRECTORY_SEPARATOR. $this->fileName($file));

        }elseif (Settings::getValue('loading_image') == '2'){
            return $this->sendExternalServer(
                $file,
                'wert2801.ftp.tools',
                'wert2801_test',
                'st9lDZK304dY'
            );

        }else{
            return false;
        }

    }

    /*
     * В зависмости от настроек системы указываем путь хранения картинок
     */
    public function fileName($file)
    {
        return  $this->booksId.'_'.$file->baseName . '.' . $file->extension;
    }

    /**
     * Загружаем файл на удаленный сервер
     * @param $file
     * @param $ftp_server
     * @param $ftp_user_name
     * @param $ftp_user_pass
     * @return bool
     */
    public function sendExternalServer($file, $ftp_server, $ftp_user_name, $ftp_user_pass)
    {
        $conn_id = ftp_connect($ftp_server);
        // входим при помощи логина и пароля
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
        if (!$conn_id || !$login_result) {
           return false;
        }
        // загружаем файл
        $upload = ftp_put($conn_id, '/uploads/'.$this->fileName($file), $file->tempName,  FTP_BINARY);
        if ($upload) {
           return true;
        }

        return false;
    }


}