<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%publishing}}".
 *
 * @property int $publishing_id
 * @property string $publishing_name названия издательства
 * @property string $publishing_address адрес издательства
 * @property string $address_phone телефон издательства
 *
 * @property Books[] $books
 */
class Publishing extends \yii\db\ActiveRecord
{
    const CACHE_TIME = 15;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%publishing}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['publishing_name', 'publishing_address', 'address_phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'publishing_id' => ' ID',
            'publishing_name' => 'Name',
            'publishing_address' => 'Address',
            'address_phone' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['books_publishing_id' => 'publishing_id']);
    }

    public static function getAll()
    {
        return Yii::$app->db->createCommand(
            'SELECT *
                  FROM ' . static::tableName()
        )->queryAll();

    }

    public static function getList()
    {
        $data = Yii::$app->cache->getOrSet('publishing_get_list', function () {
            return ArrayHelper::map(static::getAll(), 'publishing_id', 'publishing_name');
        }, self::CACHE_TIME);

        return $data;
    }
}
