<?php

namespace app\models;

use app\helpers\AdjacencyListIterator;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%rubrics}}".
 *
 * @property int $rubrics_id
 * @property int $rubrics_parent_id id родителя рубрики
 * @property string $rubrics_name название рубрики
 *
 * @property Books[] $books
 */
class Rubrics extends \yii\db\ActiveRecord
{
    public static $parentName;
    public static $items = [];

    const MAIN_RUBRICS_ID = 0;
    const CACHE_TIME = 15;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%rubrics}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rubrics_parent_id'], 'integer'],
            [['rubrics_name'], 'required'],
            [['rubrics_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rubrics_id' => 'ID',
            'rubrics_parent_id' => 'Rubrics Parent',
            'rubrics_name' => 'Rubrics Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['books_rubrics_id' => 'rubrics_id']);
    }


    /*
     * Список категорий
     */
    public static function getList(): array
    {
        if (static::$items == null) {
            static::$items = Yii::$app->cache->getOrSet('rubrics_get_list', function () {
                return static::getListGroup();
            }, self::CACHE_TIME);

            return static::$items;
        }

        return static::$items;
    }

    /**
     *  Рекурсивная функция для получения массива категорий с визуальной вложенностью
     */
    public static function getListGroup($indent = '', $parent_group = 0): array
    {
        $groups = self::find()
            ->where(['rubrics_parent_id' => $parent_group])
            ->orderBy('rubrics_name')
            ->asArray()
            ->all();

        foreach ($groups as $group) {
            static::$items[$group['rubrics_id']] = $indent . $group['rubrics_name'];
            array_merge(static::$items, self::getListGroup($indent . '-', $group['rubrics_id']));
        }

        return static::$items;
    }


    /*
     * Выводим имена родительских категорий рубрик
     */
    public static function getParentName(int $id): string
    {
        if ($id == self::MAIN_RUBRICS_ID)
            return 'Без рубрики';

        if (static::$parentName == null) {
            $parents = self::find()
                ->select(['rubrics_id', 'rubrics_name'])
                ->asArray()
                ->all();
            static::$parentName = ArrayHelper::map($parents, 'rubrics_id', 'rubrics_name');
        }

        return static::$parentName[$id] ?? '';
    }


}
