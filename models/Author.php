<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%author}}".
 *
 * @property int $author_id
 * @property string $author_name
 *
 * @property Books[] $books
 */
class Author extends \yii\db\ActiveRecord
{
    const CACHE_TIME = 15;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%author}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'author_id' => 'Author ID',
            'author_name' => 'Author Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['books_author_id' => 'author_id']);
    }

    public static function getAll()
    {
        return Yii::$app->db->createCommand(
            'SELECT *
                  FROM ' . static::tableName()
        )->queryAll();

    }

    public static function getList()
    {
        $data = Yii::$app->cache->getOrSet('author_get_list', function () {
            return ArrayHelper::map(static::getAll(), 'author_id', 'author_name');
        }, self::CACHE_TIME);

        return $data;
    }
}
