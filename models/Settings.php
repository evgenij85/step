<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%settings}}".
 *
 * @property int $settings_id
 * @property string $settings_key
 * @property string $settings_value
 * @property string $settings_description
 */
class Settings extends \yii\db\ActiveRecord
{
    public static $getValue;

    const CACHE_TIME = 15;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['settings_key', 'settings_value'], 'required'],
            [['settings_key', 'settings_value', 'settings_description'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'settings_id' => 'Settings ID',
            'settings_key' => 'Settings Key',
            'settings_value' => 'Settings Value',
            'settings_description' => 'Settings Description',
        ];
    }

    /**
     * Получаем массив со всеми данными таблицы settings\yii\db\Exception
     */
    public static function getAll(): array
    {
        return Yii::$app->db->createCommand(
            'SELECT *
                  FROM ' . static::tableName()
        )->queryAll();

    }

    /**
     * Получаем значение таблицы settings по ключу
     */
    public static function getValue(string $key): string
    {
        if (static::$getValue == null) {
            static::$getValue = ArrayHelper::map(static::getAll(), 'settings_key', 'settings_value');
        }
        return static::$getValue[$key] ?? '';

    }


    /*
     * Проверяем активность опции, если паратметри соотвествует возвращаем успешно
     */
    public static function activityValues(string $key, string $value): bool
    {
        if (static::getValue($key) == $value) {
            return true;
        }
        return false;

    }

    /*
     * Обновляем значение параметров
     */
    public static function updateValues(string $key, string $newValue): bool
    {
        if ($model = self::findOne(['settings_key' => $key])) {
            $model->settings_value = $newValue;
            if ($model->save()) {
                return true;
            }
        }
        return false;

    }

}
