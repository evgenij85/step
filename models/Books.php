<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%books}}".
 *
 * @property int $books_id
 * @property int $books_rubrics_id id рубрики
 * @property int $books_author_id id автора книги
 * @property int $books_publishing_id id издательства
 * @property string $books_name название книги
 * @property string $books_date_publishing дата издательства
 *
 * @property Author $booksAuthor
 * @property Publishing $booksPublishing
 * @property Rubrics $booksRubrics
 */
class Books extends \yii\db\ActiveRecord
{
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%books}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['books_rubrics_id', 'books_author_id', 'books_publishing_id', 'books_name'], 'required'],
            [['books_rubrics_id', 'books_author_id', 'books_publishing_id'], 'integer'],
            [['books_date_publishing'], 'safe'],
            [['books_date_publishing'], 'default', 'value' => \Yii::$app->formatter->asDate('now')],
            [['books_name'], 'string', 'max' => 255],
            [['books_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Author::className(), 'targetAttribute' => ['books_author_id' => 'author_id']],
            [['books_publishing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Publishing::className(), 'targetAttribute' => ['books_publishing_id' => 'publishing_id']],
            [['books_rubrics_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubrics::className(), 'targetAttribute' => ['books_rubrics_id' => 'rubrics_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'books_id' => 'ID',
            'books_rubrics_id' => 'Rubrics',
            'books_author_id' => 'Author',
            'books_publishing_id' => 'Publishing',
            'books_name' => 'Name',
            'books_date_publishing' => 'Date Publishing',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooksAuthor()
    {
        return $this->hasOne(Author::className(), ['author_id' => 'books_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooksPublishing()
    {
        return $this->hasOne(Publishing::className(), ['publishing_id' => 'books_publishing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooksRubrics()
    {
        return $this->hasOne(Rubrics::className(), ['rubrics_id' => 'books_rubrics_id']);
    }

    public function getBooksPictures()
    {
        return $this->hasMany(Pictures::className(), ['pictures_books_id' => 'books_id']);
    }
}
