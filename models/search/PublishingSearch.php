<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Publishing;

/**
 * PublishingSearch represents the model behind the search form of `app\models\Publishing`.
 */
class PublishingSearch extends Publishing
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['publishing_id'], 'integer'],
            [['publishing_name', 'publishing_address', 'address_phone'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Publishing::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'publishing_id' => $this->publishing_id,
        ]);

        $query->andFilterWhere(['like', 'publishing_name', $this->publishing_name])
            ->andFilterWhere(['like', 'publishing_address', $this->publishing_address])
            ->andFilterWhere(['like', 'address_phone', $this->address_phone]);

        return $dataProvider;
    }
}
