<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rubrics;

/**
 * RubricsSearch represents the model behind the search form of `app\models\Rubrics`.
 */
class RubricsSearch extends Rubrics
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rubrics_id', 'rubrics_parent_id'], 'integer'],
            [['rubrics_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rubrics::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rubrics_id' => $this->rubrics_id,
            'rubrics_parent_id' => $this->rubrics_parent_id,
        ]);

        $query->andFilterWhere(['like', 'rubrics_name', $this->rubrics_name]);

        return $dataProvider;
    }
}
