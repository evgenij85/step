<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Books;

/**
 * BooksSearch represents the model behind the search form of `app\models\Books`.
 */
class BooksSearch extends Books
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['books_id', 'books_rubrics_id', 'books_author_id', 'books_publishing_id'], 'integer'],
            [['books_name', 'books_date_publishing'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Books::find()->with('booksAuthor','booksPublishing', 'booksRubrics');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'books_id' => $this->books_id,
            'books_rubrics_id' => $this->books_rubrics_id,
            'books_author_id' => $this->books_author_id,
            'books_publishing_id' => $this->books_publishing_id,
            'books_date_publishing' => $this->books_date_publishing,
        ]);

        $query->andFilterWhere(['like', 'books_name', $this->books_name]);

        return $dataProvider;
    }
}
