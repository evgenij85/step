<?php

/* @var $this yii\web\View */

$this->title = 'Test step';

$this->registerJsFile('http://code.jquery.com/jquery-latest.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<!-- Content -->
<div class="bg-12 p-t-100 p-b-70">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-md-4 col-lg-3 p-b-30">
                <!-- Left bar -->
                <?php include_once __DIR__.'/../include/left_menu.php'?>
            </div>

            <div class="col-sm-10 col-md-8 col-lg-9 p-b-30">
                <div class="p-l-20 p-l-0-sr767">
                    <div class="row">

                        <?php foreach ($books as $book):?>
                        <div class="col-lg-6 p-b-50">
                            <div class="bg-0 h-full">
                                <a href="#" class="hov-img0 of-hidden">
                                    <!-- <img src="images/news-01.jpg" alt="IMG">-->
                                    <div class="slider">
                                        <ul>
                                            <?php foreach ($book->booksPictures as $p):?>
                                            <li><img src="<?= \app\models\Pictures::dirLocation($p->pictures_server).$p->pictures_name ?>" alt=""></li>
                                            <?php endforeach;?>
                                        </ul>
                                    </div>

                                </a>

                                <div class="bg-0 p-rl-28 p-t-26 p-b-35">
                                    <h4 class="p-b-12">
                                        <a href="#" class="t1-m-1 cl-3 hov-link2 trans-02">
                                            <?=$book->books_name?>
                                        </a>
                                    </h4>

                                    <div class="flex-wr-s-c p-b-9">
                                        <div class="p-r-20">
                                            <i class="fs-14 cl-7 fa fa-calendar m-r-2"></i>

                                            <span class="t1-s-2 cl-7" title="Дата публикации">
													 <?=$book['books_date_publishing']?>
												</span>
                                        </div>

                                        <div class="p-l-20 bo-l-1 bcl-12">
                                            <i class="fs-14 cl-7 fa fa-user m-r-2"></i>

                                            <a href="#" class="t1-s-2 cl-11 hov-link3" title="Автор">
                                                <?=$book->booksAuthor->author_name?>
                                            </a>
                                        </div>
                                    </div>

                                    <p class="t1-s-2 cl-6 p-b-20">
                                    <ul>
                                        <li>Hазвание: <b><?=$book->booksPublishing->publishing_name?></b></li>
                                        <li>Aдрес: <b><?=$book->booksPublishing->publishing_address?></b></li>
                                        <li>Tелефон: <b><?=$book->booksPublishing->address_phone?></b></li>
                                        <li>Рубрика: <b><?=$book->booksRubrics->rubrics_name?></b></li>
                                    </ul>
                                    </p>

                                    <a href="#" class="d-inline-flex flex-c-c size-a-1 p-rl-15 t1-s-2 text-uppercase cl-0 bg-11 hov-btn1 trans-02">
                                        Read More
                                    </a>
                                </div>
                            </div>
                        </div>
            <?php endforeach;?>
                    </div>

                    <!-- Pagination -->
                    <div class="flex-wr-s-c p-t-7">

                        <?= \yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                        ]); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
$js = <<<JS
 
 $(".slider").each(function () { // обрабатываем каждый слайдер
  var obj = $(this);
  $(obj).append("<div class='nav'></div>");
  $(obj).find("li").each(function () {
   $(obj).find(".nav").append("<span rel='"+$(this).index()+"'></span>"); // добавляем блок навигации
   $(this).addClass("slider"+$(this).index());
  });
  $(obj).find("span").first().addClass("on"); // делаем активным первый элемент меню
 });

function sliderJS (obj, sl) { // slider function
 var ul = $(sl).find("ul"); // находим блок
 var bl = $(sl).find("li.slider"+obj); // находим любой из элементов блока
 var step = $(bl).width(); // ширина объекта
 $(ul).animate({marginLeft: "-"+step*obj}, 500); // 500 это скорость перемотки
}
$(document).on("click", ".slider .nav span", function() { // slider click navigate
 var sl = $(this).closest(".slider"); // находим, в каком блоке был клик
 $(sl).find("span").removeClass("on"); // убираем активный элемент
 $(this).addClass("on"); // делаем активным текущий
 var obj = $(this).attr("rel"); // узнаем его номер
 sliderJS(obj, sl); // слайдим
 return false;
});

JS;
$this->registerJs($js, \yii\web\View::POS_END);
?>