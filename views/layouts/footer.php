<!-- Footer -->
<footer>
    <div class="parallax100 kit-overlay1 p-t-35 p-b-15" style="background-image: url(images/bg-03.jpg);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-8 col-md-5 col-lg-3 p-b-20">
                    <div class="size-h-1 flex-s-e p-b-6 m-b-18">
                        <a href="#">
                            <img class="max-s-full" src="images/icons/logo-02.png" alt="IMG">
                        </a>
                    </div>

                    <div>
                        <p class="t1-s-2 cl-13 p-b-9">
                            On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized   the charms of pleasure of.
                        </p>
                    </div>
                </div>

                <div class="col-sm-8 col-md-5 col-lg-3 p-b-20">
                    <div class="size-h-1 flex-s-e m-b-18">
                        <h4 class="t1-m-3 text-uppercase cl-0">
                            Contact us
                        </h4>
                    </div>

                    <ul>
                        <li class="flex-wr-s-s t1-s-2 cl-13 p-b-9">
								<span class="size-w-3">
									<i class="fa fa-home" aria-hidden="true"></i>
								</span>

                            <span class="size-w-4">
									379 5Th Ave New York, Nyc 10018
								</span>
                        </li>

                        <li class="flex-wr-s-s t1-s-2 cl-13 p-b-9">
								<span class="size-w-3">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
								</span>

                            <span class="size-w-4">
									contac@gmail.com
								</span>
                        </li>

                        <li class="flex-wr-s-s t1-s-2 cl-13 p-b-9">
								<span class="size-w-3">
									<i class="fa fa-phone" aria-hidden="true"></i>
								</span>

                            <span class="size-w-4">
									(+1) 96 716 6879
									<br>
									(+1) 96 716 6897
								</span>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-8 col-md-5 col-lg-3 p-b-20">
                    <div class="size-h-1 flex-s-e m-b-18">
                        <h4 class="t1-m-3 text-uppercase cl-0">
                            Company
                        </h4>
                    </div>

                    <div class="flex-wr-s-s">
                        <ul class="w-50">
                            <li class="kit-list1 p-b-9">
                                <a href="index.html" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    Home
                                </a>
                            </li>

                            <li class="kit-list1 p-b-9">
                                <a href="projects-grid.html" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    Projects
                                </a>
                            </li>

                            <li class="kit-list1 p-b-9">
                                <a href="services-list.html" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    Services
                                </a>
                            </li>

                            <li class="kit-list1 p-b-9">
                                <a href="about.html" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    About us
                                </a>
                            </li>

                            <li class="kit-list1 p-b-9">
                                <a href="contact.html" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    Contact
                                </a>
                            </li>
                        </ul>

                        <ul class="w-50">
                            <li class="kit-list1 p-b-9">
                                <a href="#" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    Blogs
                                </a>
                            </li>

                            <li class="kit-list1 p-b-9">
                                <a href="#" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    404 Page
                                </a>
                            </li>

                            <li class="kit-list1 p-b-9">
                                <a href="shop-grid.html" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    Shop
                                </a>
                            </li>

                            <li class="kit-list1 p-b-9">
                                <a href="#" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    Elements
                                </a>
                            </li>

                            <li class="kit-list1 p-b-9">
                                <a href="typography.html" class="t1-s-2 cl-13 hov-link2 trans-02">
                                    Typography
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-8 col-md-5 col-lg-3 p-b-20">
                    <div class="size-h-1 flex-s-e m-b-18">
                        <h4 class="t1-m-3 text-uppercase cl-0">
                            subscribe
                        </h4>
                    </div>

                    <div class="p-t-6">
                        <form class="flex-wr-c-c m-b-15">
                            <input class="size-a-17 bg-0 t1-s-2 plh-6 cl-6 p-rl-20" type="text" name="email" placeholder="Your E-mail">

                            <button class="flex-c-c size-a-16 bg-11 fs-18 cl-10 hov-btn3 trans-02">
                                <i class="fa fa-envelope-o"></i>
                            </button>
                        </form>

                        <div class="flex-wr-s-c p-t-10">
                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10">
                                <i class="fa fa-facebook"></i>
                            </a>

                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10">
                                <i class="fa fa-twitter"></i>
                            </a>

                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10">
                                <i class="fa fa-google-plus"></i>
                            </a>

                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10">
                                <i class="fa fa-instagram"></i>
                            </a>

                            <a href="#" class="flex-c-c size-a-7 borad-50per bg-11 fs-16 cl-0 hov-btn2 trans-02 m-r-10">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-10">
        <div class="container txt-center p-tb-15">
				<span class="t1-s-2 cl-14">
					Copyright @ 2018 Designed by pencilblue. All rights reserved.
				</span>
        </div>
    </div>
</footer>