<div>
    <!-- Search -->
    <form class="flex-wr-c-c m-b-45">
        <input class="size-a-21 bg-none t1-s-5 cl-6 plh-6 bo-tbl-1 bcl-11 p-rl-20" type="text" name="search" placeholder="Search...">

        <button class="size-a-22 fs-18 cl-0 bg-11 hov-btn1 trans-02">
            <i class="fa fa-search"></i>
        </button>
    </form>

    <!-- Categories -->
    <div class="p-b-32">
        <h4 class="t1-m-1 text-uppercase cl-3 kit-underline1 p-b-6">
            Categories
        </h4>

        <ul class="p-t-22">
            <li class="bo-b-1 bcl-14 m-b-18">
                <a href="#" class="flex-wr-sb-c t1-s-5 cl-5 hov-link2 trans-02 p-tb-3">
										<span>
											Business
										</span>

                    <span>
											30
										</span>
                </a>
            </li>

            <li class="bo-b-1 bcl-14 m-b-18">
                <a href="#" class="flex-wr-sb-c t1-s-5 cl-5 hov-link2 trans-02 p-tb-3">
										<span>
											Training
										</span>

                    <span>
											10
										</span>
                </a>
            </li>

            <li class="bo-b-1 bcl-14 m-b-18">
                <a href="#" class="flex-wr-sb-c t1-s-5 cl-5 hov-link2 trans-02 p-tb-3">
										<span>
											Security
										</span>

                    <span>
											23
										</span>
                </a>
            </li>

            <li class="bo-b-1 bcl-14 m-b-18">
                <a href="#" class="flex-wr-sb-c t1-s-5 cl-5 hov-link2 trans-02 p-tb-3">
										<span>
											Stock Market
										</span>

                    <span>
											17
										</span>
                </a>
            </li>
        </ul>
    </div>

</div>